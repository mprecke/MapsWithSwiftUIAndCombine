//
//  Location.swift
//  MapsWithSwiftUI
//
//  Created by Moritz Philip Recke on 26.02.21.
//

import Foundation
import CoreLocation
import MapKit

struct Location: Identifiable, Hashable {
    
    let id = UUID()
    var location: CLLocation = CLLocation()
    var placemark: CLPlacemark?
    var mapItem: MKMapItem = MKMapItem()
}
