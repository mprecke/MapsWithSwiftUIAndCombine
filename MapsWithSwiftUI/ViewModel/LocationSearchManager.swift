//
//  LocationSearchManager.swift
//  MapsWithSwiftUI
//
//  Created by Moritz Philip Recke on 26.02.21.
//

import Foundation
import MapKit
import Combine

class LocationSearchManager: NSObject, ObservableObject {

    @Published var queryFragment: String = ""
    @Published private(set) var status: LocationSearchStatus = .idle
    @Published var localSearchResults : [Location] = []

    private var queryCancellable: AnyCancellable?
 
    enum LocationSearchStatus: Equatable {
        case idle
        case searching
        case result
        case noResult
        case error(String)
    }
    
    override init() {
        super.init()

        queryCancellable = $queryFragment
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { fragment in
                self.localSearchResults = []
                self.status = .searching
                if !fragment.isEmpty && fragment.count > 3 {
                    let searchRequest = MKLocalSearch.Request()
                    searchRequest.naturalLanguageQuery = fragment
                    let search = MKLocalSearch(request: searchRequest)
                    search.start { response, error in
                        guard let response = response else {
                            self.status = .error("Error: \(error?.localizedDescription ?? "Unknown error").")
                            return
                        }
                        for item in response.mapItems {
                            self.localSearchResults.append(Location(location: item.placemark.location ?? CLLocation(), placemark: item.placemark, mapItem: item))
                        }
                        self.status = .result
                    }
                } else if !fragment.isEmpty && fragment.count <= 3 {
                    self.localSearchResults = []
                    self.status = .noResult
                } else {
                    self.localSearchResults = []
                    self.status = .idle
                }
        })
    }
}
