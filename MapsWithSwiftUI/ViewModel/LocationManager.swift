//
//  LocationManager.swift
//  MapsWithSwiftUI
//
//  Created by Moritz Philip Recke on 26.02.21.
//

import Foundation
import CoreLocation
import Combine
import UIKit

class LocationManager: NSObject, ObservableObject {
    
    private let locationManager = CLLocationManager()
    
    @Published var status: CLAuthorizationStatus?
    @Published var location: CLLocation?
    
    private enum LocationProviderError: Error {
        case noAuthorization
    }
    
    private let allowedAuthorizationTypes : Set<CLAuthorizationStatus> = Set([.authorizedWhenInUse, .authorizedAlways])
    
    override init() {
        super.init()
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.activityType = .fitness
        self.locationManager.allowsBackgroundLocationUpdates = true
        self.locationManager.pausesLocationUpdatesAutomatically = false
        self.locationManager.showsBackgroundLocationIndicator = true
        
        do {
            try start()
            
        } catch {
            print("No location access.")
            self.requestAuthorization()
        }

    }
    
    public func start() throws -> Void {
        self.requestAuthorization()
    
        if let status = self.status {
            guard allowedAuthorizationTypes.contains(status) else {
                throw LocationProviderError.noAuthorization
            }
        }
        self.locationManager.startUpdatingLocation()
    }
    
    public func stop() -> Void {
        self.locationManager.stopUpdatingLocation()
    }
    
    
    public func requestAuthorization(authorizationRequestType: CLAuthorizationStatus = CLAuthorizationStatus.authorizedWhenInUse) -> Void {
        if self.status == CLAuthorizationStatus.denied {
            showLocationSettingsAlert()
        }
        else {
            switch authorizationRequestType {
            case .authorizedWhenInUse:
                self.locationManager.requestWhenInUseAuthorization()
                self.locationManager.startUpdatingLocation()
            case .authorizedAlways:
                self.locationManager.requestAlwaysAuthorization()
                self.locationManager.startUpdatingLocation()
            default: break
            }
        }
    }
    
    public func showLocationSettingsAlert(alertText : String? = nil) -> Void {
        let alertController = UIAlertController (title: "Allow Location Access", message: alertText ?? "Location access has been denied for this app. To use all features of the app, please anable lcoation access in the app's settings.", preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string:UIApplication.openSettingsURLString) else {
                return
            }
            UIApplication.shared.open(settingsUrl)
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        UIApplication.shared.windows[0].rootViewController?.present(alertController, animated: true, completion: nil)
    }
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.status = status
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        self.location = location
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if let clErr = error as? CLError {
            switch clErr {
            case CLError.denied : do {
                print(#function, "User denied Location access.")
                self.locationManager.stopUpdatingLocation()
                self.requestAuthorization()
            }
            case CLError.locationUnknown : print(#function, "Cannot retrieve location.")
            default: print(#function, "Location retrieval failed, error unknown.")
            }
        }
        else {
            print(#function, "Location retrieval failed, error unknown", error.localizedDescription)
        }
    }
}

