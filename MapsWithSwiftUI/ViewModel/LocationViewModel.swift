//
//  LocationViewModel.swift
//  MapsWithSwiftUI
//
//  Created by Moritz Philip Recke on 26.02.21.
//

import Foundation
import Combine

class LocationViewModel: ObservableObject {

    @Published var location: Location
    
    var latitude: Double {
        location.location.coordinate.latitude
    }
    
    var longitude: Double {
        location.location.coordinate.longitude
    }
    
    init() {
        self.location = Location()
    }
    
    
}
