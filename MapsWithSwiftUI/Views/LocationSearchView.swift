//
//  LocationSearchView.swift
//  MapsWithSwiftUI
//
//  Created by Moritz Philip Recke on 26.02.21.
//

import SwiftUI
import MapKit
import Combine

struct LocationSearchView: View {
    
    @StateObject var locationSearch = LocationSearchManager()
    
    var body: some View {
        
        NavigationView {
            Form {
                Section(header: Text("Location Search")) {
                    ZStack(alignment: .trailing) {
                        TextField("Search", text: $locationSearch.queryFragment)
                        if locationSearch.status == .searching {
                            Image(systemName: "clock")
                                .foregroundColor(Color.gray)
                        }
                    }
                }
                Section(header: Text("Search Results")) {
                    List {
                        Group { () -> AnyView in
                            switch locationSearch.status {
                            case .noResult:
                                return AnyView(Text("No Results, please extend your query"))
                            case .error(let description):
                                return AnyView(Text("Error: \(description)"))
                            default:
                                return AnyView(EmptyView())
                            }
                        }.foregroundColor(Color.gray)
                        
                        ForEach(locationSearch.localSearchResults, id: \.self) { result in
                            NavigationLink(destination: LocationSearchDetailView(location: result, region: MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: result.location.coordinate.latitude, longitude: result.location.coordinate.longitude) , span: MKCoordinateSpan(latitudeDelta: 0.0015, longitudeDelta: 0.0015)), places: [PointOfInterest(name: result.placemark?.name ?? "no name", latitude: result.location.coordinate.latitude, longitude: result.location.coordinate.longitude)])) {
                                
                                Text((result.placemark?.name ?? "no name") + (", \(result.placemark?.postalCode ?? "no ZIP")") + (", \(result.placemark?.locality ?? "no city")"))
                                
                            }
                        }
                        
                    }
                }
            }
            .navigationBarTitle("Points of Interest", displayMode: .automatic)
        }
        
    }
}

struct LocationSearchView_Previews: PreviewProvider {
    static var previews: some View {
        LocationSearchView()
    }
}
