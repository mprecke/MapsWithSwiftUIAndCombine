//
//  CustomMapView.swift
//  MapsWithSwiftUI
//
//  Created by Moritz Philip Recke on 26.02.21.
//

import SwiftUI
import MapKit

struct CustomMapView: View {
    
    @State private var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 40.83834587046632, longitude: 14.254053016537693) , span: MKCoordinateSpan(latitudeDelta: 0.075, longitudeDelta: 0.075))
    
    @State private var places: [PointOfInterest] = []
    
    var body: some View {
        ZStack {
            Map(coordinateRegion: $region, annotationItems: places) { place in
                MapMarker(coordinate: place.coordinate)
            }
            Circle()
                .fill(Color.red)
                .opacity(0.5)
                .frame(width: 15, height: 15)
            VStack {
                Spacer()
                HStack {
                    Spacer()
                    Button(action: {
                        places.append(PointOfInterest(name: "Pin No \(places.count + 1)", latitude: region.center.latitude, longitude: region.center.longitude))
                    }) {
                        Image(systemName: "plus")
                    }
                    .frame(width: 30, height:30)
                    .padding()
                    .background(Color.black.opacity(0.7))
                    .foregroundColor(.white)
                    .font(.title)
                    .clipShape(Circle())
                    .padding(20)
                    .offset(y: -100)
                }
            }
        }
        .edgesIgnoringSafeArea(.all)
    }
}

struct CustomMapView_Previews: PreviewProvider {
    static var previews: some View {
        CustomMapView()
    }
}
