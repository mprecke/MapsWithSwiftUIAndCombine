//
//  LocationView.swift
//  MapsWithSwiftUI
//
//  Created by Moritz Philip Recke on 26.02.21.
//

import SwiftUI
import MapKit
import Combine

struct LocationView: View {
    
    @State var locationManager = LocationManager()
    
    @State private var viewModel = LocationViewModel()
    
    @State private var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 40.83834587046632, longitude: 14.254053016537693) , span: MKCoordinateSpan(latitudeDelta: 0.075, longitudeDelta: 0.075))
    
    @State private var userTrackingMode: MapUserTrackingMode = .follow
    
    var body: some View {
        ZStack {
            Map(coordinateRegion: $region, interactionModes: MapInteractionModes.all,
                showsUserLocation: true, userTrackingMode: $userTrackingMode)
                .onAppear(){
                    updateRegion()
                }
            VStack {
                Spacer()
                HStack {
                    Spacer()
                    Button(action: {
                        updateRegion()
                    }) {
                        Image(systemName: "location.fill")
                    }
                    .frame(width: 30, height:30)
                    .padding()
                    .background(Color.black.opacity(0.7))
                    .foregroundColor(.white)
                    .font(.title)
                    .clipShape(Circle())
                    .padding(20)
                    .offset(y: -100)
                }
            }
        }.ignoresSafeArea()
        .onReceive(Publishers.CombineLatest.init(locationManager.$status, locationManager.$location), perform: { status, loc  in
                self.viewModel.location.location = loc ?? CLLocation()
            })
    }
    
    func updateRegion() {
        region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: viewModel.latitude, longitude: viewModel.longitude) , span: MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005))
    }
}

struct LocationView_Previews: PreviewProvider {
    static var previews: some View {
        LocationView()
    }
}
