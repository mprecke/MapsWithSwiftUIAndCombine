//
//  LocationSearchDetailView.swift
//  MapsWithSwiftUI
//
//  Created by Moritz Philip Recke on 26.02.21.
//

import SwiftUI
import MapKit


struct LocationSearchDetailView: View {
    
    @State var location: Location
    
    @State var region: MKCoordinateRegion
    @State var places: [PointOfInterest]
    
    var body: some View {
        VStack{
            Map(coordinateRegion: $region, annotationItems: places) { place in
                MapPin(coordinate: place.coordinate)
            }
            .frame(height: 300)
            VStack(alignment: .leading, spacing: 8){
                Text("Contact Information")
                    .font(.title2)
                Divider()
                Text((location.placemark?.thoroughfare ?? "no street address") + (" \(location.placemark?.subThoroughfare ?? "no house number")"))
                Text((location.placemark?.postalCode ?? "no ZIP") + (" \(location.placemark?.locality ?? "no locality")"))
                Text(location.placemark?.country ?? "no country")
                Text("Phone: \(location.mapItem.phoneNumber ?? "no phone number")")
                Spacer()
            }
            .padding()
        }
        .navigationTitle(location.placemark?.name ?? "no name")
        .navigationBarTitleDisplayMode(.automatic)
    }
}

struct LocationSearchDetailView_Previews: PreviewProvider {
    static var previews: some View {
        LocationSearchDetailView(location: Location(), region: MKCoordinateRegion(), places: [])
    }
}
