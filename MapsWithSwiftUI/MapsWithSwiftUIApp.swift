//
//  MapsWithSwiftUIApp.swift
//  MapsWithSwiftUI
//
//  Created by Moritz Philip Recke on 26.02.21.
//

import SwiftUI

@main
struct MapsWithSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
