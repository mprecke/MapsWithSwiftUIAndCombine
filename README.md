# MapKit and CoreLocation with SwiftUI and Combine

This is a simple implementation of MapKit and CoreLocation with SwiftUI and Combine. The project uses CoreLocation to obtain the users location and uses MKLocalSearch to search for Points of Interest on Apple Maps. Beyond that, simple features of MapKit in SwiftUI are showcased. 

It is based on the MapKit integration within SwiftUI presented at WWDC 2020 and does not use any UIViewRepsentable or other UIKit elements to showcase the limited but easy to implement Map features available in SwiftUI.

